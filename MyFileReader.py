from FileReaderInterface import FileReaderInterface


class generate_file:
    def __init__(self, filename, buffer_size):
        self.filename = filename
        self.buffer_size = buffer_size
        self.file = None

    def __iter__(self):
        with open(self.filename, 'r') as file:
            while True:
                data = file.read(self.buffer_size)
                if not data:
                    break
                yield data

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        pass


class MyFileReader(FileReaderInterface):
    def read_file(self, file_path, buffer_size):
        return generate_file(file_path, int(buffer_size))
