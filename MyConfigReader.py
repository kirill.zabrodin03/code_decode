from ConfigReaderInterface import ConfigReaderInterface
from ConfigException import ConfigException

class MyConfigReader(ConfigReaderInterface):
    def read_config(self, file_path):
        config = {}

        with open(file_path, 'r') as file:
            for line in file:
                key, value = line.strip().split('=', 1)
                config[key.strip()] = value.strip()
                if key.strip() == "buffer_size":
                    try:
                        int(value.strip())
                    except ValueError:
                        raise ConfigException(f'Неверный тип значения для "buffer_size": {value.strip()}, ожидался тип int')

        return config
